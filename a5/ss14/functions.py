import math

def get_requirements():
    print("Sphere Volume Requirements\n")
    print("Program Requirements:\n"
        + "1. Program calculates two numbers, and rounds to decimal points.\n"
        + "2. Prompt user for two numbers, sutiable operator\n"
        + "3. Use Python error handling to validate data.\n"
        + "4. Test for correct arithmetic operator.\n"
        + "5. Division by zero not premitted. \n"
        + "6. Note: Program loops until correct input entered - numbers and arithmetic operator.\n")

def getNum(prompt):
    while True:
        try:
            return float(input("\n" + prompt + " "))
        except ValueError:
            print("Not a number! Try again.")

def getOp():
    validOperators = ['+','-','*','/','//','%','**']
    while True:
        op = input("\nSuitable Operators: +, -, *, /, // (integer division), % (module operator), ** (power): ")
        try:
            validOperators.index(op)
            return op
        except ValueError:
            print("Invalid operator! Try again!")

def calc():
    num1 = getNum("Enter num1: ")
    num2 = getNum("Enter num2: ")
    op = getOp()
    sum = 0.0

    if op == '+':
        sum = num1 + num2
    elif op == '-':
        sum = num1 - num2
    elif op == '*':
        sum = num1 * num2
    elif op == '**':
        sum = num1 ** num2
    elif op == '%':
        while True:
            try:
                sum = num1 % num2
                break
            except ZeroDivisionError:
                num2 = getNum("Hey! You can't divide by zero! Re-enter num2:")
    elif op == '/':
        while True:
            try:
                sum = num1 / num2
                break
            except ZeroDivisionError:
                num2 = getNum("Hey! You can't divide by zero! Re-enter num2:")
    elif op == '//':
        while True:
            try:
                sum = num1 // num2
                break
            except ZeroDivisionError:
                num2 = getNum("Hey! You can't divide by zero! Re-enter num2:")

    print("\nAnswer is " + str(round(sum,2)))
    print()