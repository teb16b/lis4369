# LIS 4369 - Extensible Enterprise Solutions

## Tyler Bartlett

### Assignment 5 Requirements:

- Use RStudio
- Complete learn_to_use_R using RStudio
- Complete lis4369_a5 using RStudio
- Display Graphs

#### README.md file should include the following items:

* RStudio 4 Panel Screenshot
* Screenshot Of Graphs
* Screenshots of SS13-15


#### Assignment Screenshots:

*Screenshot of 4 Panel R-Studio*:

![ss10](img/4panel.png)

*Screenshot of First Graph*:

![ss11](img/graph1.png)

*Screenshot of Second Graph*:

![ss12](img/graph2.png)