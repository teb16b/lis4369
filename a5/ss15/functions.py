import os

lincoln_address = """
Four score and seven years ago our fathers brought forth, upon this continent, a new nation, conceived in liberty, and dedicated to the proposition that "all men are created equal"

Now we are engaged in a great civil war, testing whether that nation, or any nation so conceived, and so dedicated, can long endure. We are met on a great battle field of that war. We have come to dedicate a portion of it, as a final resting place for those who died here, that the nation might live. This we may, in all propriety do. But, in a larger sense, we can not dedicate -- we can not consecrate -- we can not hallow, this ground-- The brave men, living and dead, who struggled here, have hallowed it, far above our poor power to add or detract. The world will little note, nor long remember what we say here; while it can never forget what they did here.

It is rather for us, the living, to stand here, we here be dedica-ted to the great task remaining before us -- that, from these honored dead we take increased devotion to that cause for which they here, gave the last full measure of devotion -- that we here highly resolve these dead shall not have died in vain; that the nation, shall have a new birth of freedom, and that government of the people by the people for the people, shall not perish from the earth.
"""

def get_requirements():
    print("File Read and Write\n")
    print("Program Requirements:\n"
        + "1. Create write_read_file subdirectory with two files: main.py and functions.py.\n"
        + "2. Use President Abraham Lincoln's Gettyburg Address: Full Text.\n"
        + "3. Write address to file.\n"
        + "4. Read address from same file.\n"
        + "5. Create Python Docstrings for functions in functions.py file.\n"
        + "6. Display Python Docstrings for each function in functions.py file.\n"
        + "7. Display full file path.\n"
        + "8. Replicate display below.\n")

def file_write():
    with open('test.txt', 'w') as writer:
        for line in lincoln_address:
            writer.write(line)

    writer.close()

def file_read():
    with open('test.txt', 'r') as reader:
        for line in reader:
            print(line, end='')
    
    print('\nFull File Path:')
    print(os.getcwd() + '\\' + reader.name)
    reader.close()

def write_read_file():
    help(write_read_file)
    help(file_write)
    help(file_read)

    file_write()
    file_read()