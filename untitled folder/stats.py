import functions as f

def main():
    f.get_requirements()
    f.get_stats()

if __name__ == "__main__":
    main()