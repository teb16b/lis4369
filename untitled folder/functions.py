import statistics as s

my_list = [37, 32, 46, 28, 37, 41, 31]

def get_requirements():
    print("\nProgram Requirements:\n"
        + "1. Calculate home interior paint cost (w/o primer).\n"
        + "2. Must use float data types.\n"
        + "3. Must use SQFT_PER_GALLON constant (350).\n"
        + "4. Must use iteration structure (aka loop).\n"
        + "5. Format, right align numbers, and round to two decimal places\n"
        + "6. Create at least five functions that are called by the program:\n"
        + "\ta. main(): calls two other functions: get_requirements() and estimate_painting_cost().\n"
        + "\tb. get_requirements(): displays the program requirements\n"
        + "\tc. estimate_painting_cost(): calculates interior home painting, and calls print function.\n"
        + "\td. print_painting_estimate(): displays painting costs.\n"
        + "\te. print_painting_percentage(): displays painting costs percentages.\n")

def get_range(my_list):
    return max(my_list) - min(my_list)

def get_stats():
    print(my_list)
    print(len(my_list))
    print(sum(my_list))
    print(min(my_list))
    print(max(my_list))
    print(get_range(my_list))
    print(s.mean(my_list))
    print(s.median(my_list))
    print(s.mode(my_list))
    print(s.variance(my_list))
    print(s.stdev(my_list))