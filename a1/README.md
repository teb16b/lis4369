# LIS 4369 - Extensible Enterprise Solutions

## Tyler Bartlett

### Assignment 1 Requirements:

1. Distributed Version Control and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links

#### README.md file should include the following items:

* Screenshot of tip calculator
* Link to a1 .ipynb file: tip_calculator.ipynb
* git commands with description

> #### Git commands w/short descriptions:

1. git init = Create an empty Git repository or reinitialize an existing one
2. git status = Show the working tree status
3. git add = Add file contents to the index
4. git commit = Record changes to the repository
5. git push = Update remote refs along with associated objects
6. git pull = Fetch from and integrate with another repository or a local branch
7. git log = Show commit logs

#### Assignment Screenshots:

*Screenshot of tip calculator running*:

![tip calculator Screenshot](img/a1_code.png)

*Screenshot of tip calculator in Jupyter Notebook:

![tip calculator jupyter](img/a1_notebook.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")
