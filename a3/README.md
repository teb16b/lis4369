# LIS 4369 - Extensible Enterprise Solutions

## Tyler Bartlett

### Assignment 3 Requirements:

1. Backward Engineer A3 with Python
2. main.py imports functions.py
3. Code output in Visual Studio Code
4. Jupyter Notebook File

#### README.md file should include the following items:

* Skillset Screenshots
* A3 Screenshot
* Jupyter Notebook of A3

#### Assignment Screenshots:

*Skillset 4*:

![ss4 Screenshot](img/ss4img.png)

*Skillset 5*:

![ss5 Screenshot](img/ss5img.png)

*Skillset 6*:

![ss6 Screenshot](img/ss6img.png)

*Assignment 3*:

![a3 Screenshot](img/a3img.png)

*Jupyter Notebook*:

![jupyter notebook Screenshot](img/jupyterimg.png)
