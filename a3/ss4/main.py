import functions as f

def main():
    f.get_requirements()
    f.calculate_percent()

if __name__ == "__main__":
    main()