def get_requirements():
    print("Calorie Percentage")
    print("\nProgram Requirements:\n"
        + "1. Find calories per grams of fat, carbs, and protein. \n"
        + "2. Calculate percentages. \n"
        + "3. Must use float data types. \n"
        + "4. Format, right-align numbers, and round to two decimal places. \n")
        

def calculate_percent():
    FAT_CAL = 9
    CARB_CAL = 4
    PROTEIN_CAL = 4

    print("Input: ")
    fat_grams = float(input("Enter total fat grams: "))
    carb_grams = float(input("Enter total carb grams: "))
    protein_grams = float(input("Enter total protein grams: "))

    total_fat = fat_grams * FAT_CAL
    total_carb = carb_grams * CARB_CAL
    total_protein = protein_grams * PROTEIN_CAL

    total = total_fat + total_carb + total_protein

    fat_percent = (total_fat / total) * 100
    carb_percent = (total_carb / total) * 100
    protein_percent = (total_protein / total) * 100

    print("\nOutput: ")
    print("{0:<12} {1:<12} {2:<12}" .format("Type", "Calories", "Percentage"))
    print("{0:<12} {1:<12,.2f} {2:>8.2f} {3}" .format("Fat", total_fat, fat_percent, "%"))
    print("{0:<12} {1:<12,.2f} {2:>8.2f} {3}" .format("Carbs", total_carb, carb_percent, "%"))
    print("{0:<12} {1:<12,.2f} {2:>8.2f} {3}" .format("Protein", total_protein, protein_percent, "%"))