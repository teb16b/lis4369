def print_requirements():
    print("Python Looping Structures")
    print("Developer: Tyler Bartlett\n")
    print("Program Requirements: \n"
        + "1. Print while loop \n"
        + "2. Print for loops using range() function, and implicit and explicit lists. \n"
        + "3. Use break and continue statements. \n"
        + "4. Replicate display below. \n")

def do_loops():
    print("1. While loop: ")
    counter = 1
    while counter < 4:
        print (counter)
        counter += 1
    print()

    print("2. for loop: using range() function with on arg: ")
    for i in range(4):
        print(i)
    print()

    print("3. for loop: using range() function with two args:")
    for i in range(1,4):
        print(i)
    print()

    print("4. for loop: using range() function with three args (interval 2): ")
    for i in range(1,4,2):
        print(i)
    print()

    print("5. for loop: using range() function with three args (negative interval 2): ")
    for i in range(3,0,-2):
        print(i)
    print()

    print("6. for loop: using implicit list (list not assigned to a variable: ")
    for i in [1,2,3]:
        print(i)
    print()

    print("7. for loop: using implicit list of strings: ")
    states = ["Michigan", "Alabama", "Florida"]
    for state in states:
        print(state)
    print()

    print("8. for loop: using break statement (stops loop): ")
    for  state in states:
        if state == "Alabama":
            break
        print(state)
    print()

    print("9. for loop: using continue statement (stops and continues with next): ")
    for state in states:
        if state == "Alabama":
            continue
        print(state)
    print()

    print("10. Print list length: ")
    print(len(states))
    print()