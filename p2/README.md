# LIS 4369 - Extensible Enterprise Solutions

## Tyler Bartlett

### Project 2 Requirements:

- Use RStudio
- Complete lis4369_p2 using RStudio
- Display Graphs

#### README.md file should include the following items:

* RStudio 4 Panel Screenshot
* Screenshot Of Graphs
* Output of P2


#### Assignment Screenshots:

*Screenshot of 4 Panel R-Studio*:

![ss10](img/4panel.png)

*Screenshot of First Graph*:

![ss11](img/graph1.png)

*Screenshot of Second Graph*:

![ss12](img/graph2.png)

[Link to P2 Output](https://bitbucket.org/teb16b/lis4369/src/master/p2/p2_output.txt)