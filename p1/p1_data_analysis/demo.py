import datetime
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from matplotlib import style 


start = datetime.datetime(2010, 1, 1)
end = datetime.datetime(2018, 10, 15)

df = pdr.DataReader("XOM", "yahoo", start, end)

first_get_rows_5 = df.head(5)
last_get_rows_5 = df.tail(5)
first_get_rows_2 = df.head(2)
last_get_rows_2 = df.head(2)

print("\nPrint number of records: ")

print(df.columns)

print("\nPrint data frame: ")
print(df)

print("\nPrint first five lines: ")
print(first_get_rows_5)

print("\nPrint last five lines: ")
print(last_get_rows_5)

print("\nPrint first 2 lines: ")
print(first_get_rows_2)

print("\nPrint last 2 lines: ")
print(last_get_rows_2)

style.use('ggplot')

df['High'].plot()
df['Adj Close'].plot()
plt.legend()
plt.show()