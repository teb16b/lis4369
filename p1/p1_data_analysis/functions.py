import datetime
import pandas_datareader as pdr
import matplotlib.pyplot as plt
from matplotlib import style 

def get_requirements():
    print("Data Analysis 1\n")

    print("Developer: Tyler Bartlett")
    print("Program Requirements: ")
    print("1. Run demo.py.")
    print("2. If errors, more than likely missing installations")
    print("3. Test Python Package Installer: pip freeze")
    print("4. Research how to do the following installations: \n"
        + "\ta. pandas (only if missing)\n"
        + "\tb. pandas-datareader (only if missing)\n"
        + "\tc. matplotlib (only if missing)")
    print("5. Create at least three functions that are called by the program: \n"
        + "\ta. main(): calls at least two other functions\n"
        + "\tb. get_requirements(): displays the program requirements\n"
        + "\tc. data_analysis_1(): displays the following data.\n")

def data_analysis_1():

    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime(2018, 10, 15)

    df = pdr.DataReader("XOM", "yahoo", start, end)

    print("Print number of records: ")
    print(df.shape[0])

    first_get_rows_5 = df.head(5)
    last_get_rows_5 = df.tail(5)
    first_get_rows_2 = df.head(2)
    last_get_rows_2 = df.head(2)

    print("\nPrint number of records: ")

    print(df.columns)

    print("\nPrint data frame: ")
    print(df)

    print("\nPrint first five lines: ")
    print(first_get_rows_5)

    print("\nPrint last five lines: ")
    print(last_get_rows_5)

    print("\nPrint first 2 lines: ")
    print(first_get_rows_2)

    print("\nPrint last 2 lines: ")
    print(last_get_rows_2)

    style.use('ggplot')

    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()