# LIS 4369 - Extensible Enterprise Solutions

## Tyler Bartlett

### Project 1 Requirements:

- Run demo.py
- Correct Installations
- Test: pip freeze
- Research the following:
	- pandas
	- pandas-datareader
	- matplotlib
- Create 3 functions
	- main()
	- get_requirements()
	- data_analysis_1()

#### README.md file should include the following items:

* Jupyter Notebook Screenshot
* Visual Studio Code Screenshot
* Screenshot Of Graph
* Screenshots of SS7 - 9

#### Assignment Screenshots:

*Screenshot SS7*:

![ss7](img/ss7.png)

*Screenshot SS8*:

![ss8](img/ss8.png)

*Screenshot SS9*:

![ss9](img/ss9.png)
![ss9](img/ss9_2.png)

*Screenshot of Jupyter Notebook*:

![notebook](img/notebook1.png)
![notebook](img/notebook2.png)

*Screenshot of Data Analysis*:

![Data Analysis](img/dataanalysis1.png)
![Data Analysis](img/dataanalysis2.png)

*Screenshot of Graph*:

![Graph](img/graph.png)
