# LIS 4369 - Extensible Enterprise Solutions

## Tyler Bartlett

### Assignment 4 Requirements:

- Run demo.py
- Correct Installations
- Test: pip_freeze
- Create 3 functions
	- main()
	- get_requirements()
	- data_analysis_2()
- Display Graph

#### README.md file should include the following items:

* Jupyter Notebook Screenshot
* Visual Studio Code Screenshot
* Screenshot Of Graph
* Screenshots of SS10 - 12


#### Assignment Screenshots:

*Screenshot SS10*:

![ss10](img/ss10.png)

*Screenshot SS11*:

![ss11](img/ss11.png)

*Screenshot SS12*:

![ss12](img/ss12.png)

*Screenshot of Jupyter Notebook*:

![notebook](img/jupyter1.png)
![notebook](img/jupyter2.png)
![notebook](img/jupyter3.png)

*Screenshot of Data Analysis 2*:

![Data Analysis](img/code1.png)
![Data Analysis](img/code2.png)
![Data Analysis](img/code3.png)
![Data Analysis](img/code4.png)

*Screenshot of Graph*:

![Graph](img/graph.png)
