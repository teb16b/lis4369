# LIS 4369 - Extensive Enterprise Solutions

## Tyler Bartlett

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
	- Distributed Version Control and Bitbucket
	- Development Installations
	- Questions
	- Bitbucket repo links

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Backward Engineer A2 with Python
	- main.py imports functions.py
	- Code output in Visual Studio Code
	
3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Backward Engineer A3 with Python
	- main.py imports functions.py
	- Code output in Visual Studio Code
	- Jupyter Notebook File
4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Run demo.py
	- Correct Installations
	- Test: pip_freeze
	- Create 3 functions
		- main()
		- get_requirements()
		- data_analysis_2()
	- Display Graph

5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Use RStudio
	- Complete learn_to_use_R using RStudio
	- Complete lis4369_a5 using RStudio
	- Display Graphs
6. [P1 README.md](p1/README.md "My P1 README.md file")
	- Run demo.py
	- Correct Installations
	- Test: pip freeze
	- Research the following:
		- pandas
		- pandas-datareader
		- matplotlib
	- Create 3 functions
		- main()
		- get_requirements()
		- data_analysis_1()
7. [P2 README.md](p2/README.md "My P2 README.md file")
	- Use R-Studio
	- Screenshots of Graphs
	- Screenshot of 4 Panel
	- Link to ps_output

