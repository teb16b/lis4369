# LIS 4369 - Extensible Enterprise Solutions

## Tyler Bartlett

### Assignment 2 Requirements:

1. Backward Engineer A2 with Python
2. main.py imports functions.py
3. Code output in Visual Studio Code

#### README.md file should include the following items:

* Skillset screenshots
* A2 Payroll Code Output
* Jupyter Notebook of A2

#### Assignment Screenshots:

*Screenshot of A2*:

![A2 screenshot](img/a2_codeoutput.png)

*Screenshot of Skillset 1*:

![skillset 1 screenshot](img/skillset1.png)

*Screenshot of Skillset 2*:

![skillset 2 screenshot](img/skillset2.png)

*Screenshot of Skillset 3*:

![skillset 3 screenshot](img/skillset3.png)

